import data from './data/data.js'

let loadUsersButton = document.querySelector('.js-load');
let loadUrl = loadUsersButton.getAttribute('data-url');
let cancelButton = document.querySelector('.cancel');
const MIN_USERS = 0;
const MAX_USERS = 100;

function randomizer(arr) {
	return Math.floor(Math.random() * arr.length) + 1;
}

function getRandomUrl (url) {
	let urlRandomUser = (Math.floor(Math.random() * (MAX_USERS - MIN_USERS)) + MIN_USERS) + '';
	let arr = url.split('');
	arr.splice(arr.length - 1, 1, urlRandomUser);
	let newUrl = arr.join('');
	return newUrl;
}

function loadData (url) {
	let xhr = new XMLHttpRequest();
	xhr.responseType = 'json';

	if (xhr.status === 200) {
		//Когда заработает ajax :)
		removePreloader ();
		let data = xhr.response;
		renderCard(data);
		renderContent(statBlock, getStatistics(data), 'afterbegin');
	} else {
		console.log('error');
	}

	xhr.open('GET', url);
	xhr.send();
}

function setPreloader () {
	let body = document.querySelector('body');
	body.classList.add('loading');
}

function removePreloader () {
	let body = document.querySelector('body');
	body.classList.remove('loading');
}

cancelButton.addEventListener('click', removePreloader);

loadUsersButton.addEventListener('click', function () {
	setPreloader ();
	loadData(getRandomUrl(loadUrl));
});

function renderCard (data) {
	let users = data.results;
	let fragment = document.createDocumentFragment();
	let cardList = document.querySelector('.list');
	let template = document.querySelector('#card').content;
	let name = template.querySelector('.card__name');
	let tel = template.querySelector('.card__tel');
	let email = template.querySelector('.card__email');
	let country = template.querySelector('.country');
	let city = template.querySelector('.city');
	let street = template.querySelector('.street');
	let number = template.querySelector('.number');
	let picture = template.querySelector('.card__image');

	users.map((item) => {
		let element = template.cloneNode(true);
		picture.setAttribute('src', item.picture.large);
		name.textContent = item.name.title + ' ' + item.name.first + '.' + item.name.last;
		tel.textContent = item.cell;
		email.textContent = item.email;
		country.textContent = item.location.country;
		city.textContent = item.location.city;
		street.textContent = item.location.street.name;
		number.textContent = item.location.street.number;

		fragment.appendChild(element);
	});

	cardList.appendChild(fragment);
}

const renderContent = (container, content, place) => {
	container.insertAdjacentHTML(place, content);
};

const getStatistics = (data) => {
	return ` 
		<div>
			<span>Всего: ${data.info.results}</span>
		</div>
	`;
}

renderCard(data);

let statBlock = document.querySelector('.statistics');
renderContent(statBlock, getStatistics(data), 'afterbegin');

function countGender (data) {
	data.map((item) => {
		let male = 0;
		let female = 0;

		item.gender === 'male' ? male++ : female++;
	});
}

countGender(data.results);
